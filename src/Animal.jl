include("ValidateData.jl")
using ValidateData: input_String

abstract type Animal end

mutable struct BaseAnimal <: Animal
    name::String
    weight::UInt64
    color::String
    kind::String

    function getPathFile()
        return "./data/data_generate/$kind.txt"
    end
end
  
  
mutable struct Anfibio <: Animal
    base:: BaseAnimal
    piel::Bool
end


mutable struct Artropodo <: Animal
    base::BaseAnimal
    number_of_pairs_of_legs::UInt16
    antenas::String  
end


mutable struct Ave <: Animal
    base::BaseAnimal
    number_of_wings::UInt16
end


mutable struct Celentereo <: Animal
    base::BaseAnimal
    tentacles::UInt16
end


mutable struct Equinodermo <: Animal
    base::BaseAnimal
    tipo_estrella_erizo::String
end


mutable struct Gusano <: Animal
    base::BaseAnimal
    kind_body::String
end


mutable struct Mamifero <: Animal
    base::BaseAnimal
    cantidad_patas::UInt64
end


mutable struct Molusco <: Animal
    base::BaseAnimal
    tipo_estrella_erizo::String
end


mutable struct Pez <: Animal
    base::BaseAnimal
    scales::Bool
end


mutable struct Porifero <: Animal
    base::BaseAnimal
end


mutable struct Reptil <: Animal
    base::BaseAnimal
    tierra_mar::String
end

