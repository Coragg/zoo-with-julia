using DelimitedFiles

module FileManagement

    function read_file_csv(name_file::String)
        try
            data = readdlm(name_file, ',') # for see the row is with this way [row, Column]
            return data
        catch error
            println("Este ha sido el error que se ha producido en el sistema: ", error)
          
        end
    end


end